# name: custom-homepage
# about: Home Hero Banner
# version: 0.0.2
# authors: Sergio Marreiro

register_asset "stylesheets/common/custom-homepage.scss";
enabled_site_setting :custom_homepage_enabled;