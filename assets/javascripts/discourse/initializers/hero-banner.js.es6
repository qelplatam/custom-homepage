import { withPluginApi } from 'discourse/lib/plugin-api';
function initializePlugin(api) {
    api.registerConnectorClass("below-site-header", "custom-homepage", {
        // Setting up our component
        setupComponent(args, component) {
            // Next we're getting the site setting 'top_menu',
            // splitting the values into an array,
            // and adding a leading slash
            var topMenuRoutes =
                Discourse.SiteSettings.top_menu.split('|')
                    .map(function (route) { return '/' + route });
            // This calls our code whenever the page changes
            api.onPageChange((url) => {
                if (url === "/" || topMenuRoutes.indexOf(url)>=0) {
                    // If it's the homepage add the 'custom-homepage' class to HTML tag
                    // and set 'displayCustomHomepage' to true
                    document.querySelector("html").classList.add("custom-homepage");
                    component.set("displayCustomHomepage", true);
                } else {
                    // If we're not on the homepage remove the class
                    // and set `displayCustomHomepage` to false
                    document.querySelector("html").classList.remove("custom-homepage");
                    component.set("displayCustomHomepage", false);
                }
            });
        }
    });
}

export default {
    name: 'custom-homepage',
    initialize(container) {
        // console.log(container);
        withPluginApi('0.1', api => initializePlugin(api));
    }
};